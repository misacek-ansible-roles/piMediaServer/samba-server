Samba server (+stunnel and xinetd)
==================================

Installs samba, xinetd and stunnel to enable **mounting** samba shares to filesystem using ssl. This is not possible as a deficiency of [mount.cifs] utility with kernels < 4.11 according to [lists.samba.org]. This means that not having kernel >= 4.11 (which rhel7 and ubuntu16 doesnt have) you cannot mount remote samba volumes to filesystem with encrypted traffic.

Requirements
------------

You need root on the server where your samba volumes are.

Role Variables
--------------

Check [defaults/main.yml].

Example Playbook
----------------

Check [tests/main.yml] for example playbook:

    - hosts: all
      tasks:
        include_role:
            name: piMediaServer/samba-server
        vars:
            STUNNEL_REMOTE_HOST: 'example.com'
            STUNNEL_REMOTE_HOST_PORT: '5000'

Command line usage:

    $ export ROLES_DIR=$HOME/git/ansible/roles
    $ export ANSIBLE_CONFIG=$ROLES_DIR/ansible.cfg
    $ ansible-playbook $ROLES_DIR/piMediaServer/samba-server/tests/main.yml \
        -e STUNNEL_REMOTE_HOST=example.com \
        -e STUNNEL_REMOTE_HOST_PORT=5000

License
-------

BSD

Author Information
------------------

Written by: [Michal Nováček]


[tests/main.yml]: tests/main.yml
[Michal Nováček]: mailto:michal.novacek@gmail.com
[mount.cifs]: https://wiki.samba.org/index.php/LinuxCIFS_utils
[lists.samba.org]: https://lists.samba.org/archive/samba/2017-April/207529.html
[defaults/main.yml]: defaults/main.yml
